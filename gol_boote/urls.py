from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from account import views as otherview

from django.views.generic import TemplateView
from django.urls import path, re_path, include
from django.contrib.auth.models import User

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', otherview.index, name='index'),
    path('about/', otherview.about, name='about'),

    path('news/',  include('news.urls', namespace='news')),
    # path('donate/',  include('donate.urls', namespace='donate')),
    path('chat/',  include('chat.urls', namespace='chat')),
    path('account/',  include('account.urls', namespace='account'))

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "مدیریت سایت موسسه خیریه"
