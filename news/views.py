from account.models import Feedback
from django.shortcuts import render, get_object_or_404, redirect
from .models import Group_news, News, Cause
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def news_list(request):

    important = News.objects.all().filter(important_news=1)
    groups = Group_news.objects.all()
    news = News.objects.all()
    paginator = Paginator(news, 4)
    page = request.GET.get('page')
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)

    return render(request, 'news.html', {'page': page, 'news': news, 'groups': groups, 'important': important, "instagram_profile_name": "gologolkhoone"})


def news_important(request):
    groups = Group_news.objects.all()
    news = News.objects.all().filter(important_news=1)

    paginator = Paginator(news, 4)
    page = request.GET.get('page')
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)

    return render(request, 'news.html', {'news': news, 'page': page, 'groups': groups})


def news_category(request, slug):
    important = News.objects.all().filter(important_news=1)
    groups = Group_news.objects.all()
    group_id = get_object_or_404(Group_news, slug_group_news=slug)
    news = News.objects.all().filter(group_news=group_id)

    paginator = Paginator(news, 4)
    page = request.GET.get('page')
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)

    return render(request, 'news.html', {'important': important, 'page': page, 'news': news, 'groups': groups})


def news_detail(request, slug):
    groups = Group_news.objects.all()
    detail = get_object_or_404(News, slug_news=slug)
    news = News.objects.all().order_by("?")[:4]

    return render(request, 'news_detail.html', {'news': news, 'groups': groups, 'category': detail})


def cause_detail(request, slug):
    detail = get_object_or_404(Cause, slug_cause=slug)

    cause = Cause.objects.all().order_by("?")[:3]

    return render(request, 'cause_detail.html', {'cause': cause,  'detail': detail})


def cause_list(request):
    cause = Cause.objects.all()
    return render(request, 'cause.html', {'cause': cause})


def feedback(request):
    print(f'________{request.get_full_path()}')

    if request.method == 'POST':

        if request.POST['comment']:
            phone = Feedback()
            try:
                phone.email = request.POST['email']
            except:
                phone.user = request.user

            phone.comment = request.POST['comment']
            phone.save()
            return redirect('/')
        else:
            return redirect('/')
    else:
        return redirect('/')
