from django.urls import path
from . import views

app_name = 'news'


urlpatterns = [
    path('information/important/', views.news_important, name='news_important'),
    path('information/<slug:slug>/', views.news_detail, name='news_detail'),
    path('cause/<slug:slug>/', views.cause_detail, name='cause_detail'),
    path('cause/', views.cause_list, name='cause_list'),
    path('feedback/', views.feedback, name='feedback'),
    path('information/category/<slug:slug>/',
         views.news_category, name='news_category'),

    path('information/', views.news_list, name='news_list'),
]
