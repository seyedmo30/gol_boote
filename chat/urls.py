
from django.urls import path, re_path
from .views import index, room, mj, mk, gold


app_name = 'chat'

urlpatterns = [
    path('1/', index, name='index'),
    path('2/', gold, name='gold'),
    path('3/', mj, name='mj'),
    path('4/', mk, name='mk'),


]
