from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Group(models.Model):
    message_group = models.CharField(max_length=10, db_index=True)

    def __str__(self):
        return self.message_group


class Message(models.Model):
    author = models.ForeignKey(
        User, related_name='author_messages', on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    message_grp = models.ForeignKey(
        Group,  on_delete=models.CASCADE, related_name='message_grop', null=True, blank=True)

    def __str__(self):
        return self.author.username

    def last_10_messages(salam):
        return Message.objects.filter(message_grp=salam).order_by('-timestamp').reverse().all()[:10]
