

from django.contrib.auth.decorators import login_required, user_passes_test


def is_mj(self):
    if self.confirm_role >= 1:
        return True
    else:
        return False


mj_login_required = user_passes_test(
    lambda u: True if is_mj(u) else False, login_url='/')


def mj_login_required(view_func):
    decorated_view_func_mj = login_required(
        mj_login_required(view_func), login_url='/')
    return decorated_view_func_mj


def is_mk(self):
    if self.confirm_role >= 2:
        return True
    else:
        return False


mk_login_required = user_passes_test(
    lambda u: True if is_mk(u) else False, login_url='/')


def mj_login_required(view_func):
    decorated_view_func_mk = login_required(
        mk_login_required(view_func), login_url='/')
    return decorated_view_func_mk


def is_msool(self):
    if self.confirm_role == 3:
        return True
    else:
        return False


rec_login_required = user_passes_test(
    lambda u: True if is_msool(u) else False, login_url='/')


def msool_login_required(view_func):
    msool_view_func = login_required(
        rec_login_required(view_func), login_url='/')
    return msool_view_func
