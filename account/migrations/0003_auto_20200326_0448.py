# Generated by Django 2.2.1 on 2020-03-26 00:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_group_responsible_responsible'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customuser',
            old_name='comment_news',
            new_name='address_user',
        ),
        migrations.AddField(
            model_name='customuser',
            name='comment_user',
            field=models.CharField(max_length=400, null=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='phone_user',
            field=models.CharField(max_length=14, null=True),
        ),
    ]
