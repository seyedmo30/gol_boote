# Generated by Django 2.2.1 on 2020-03-10 11:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group_Responsible',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_group_responsible', models.CharField(db_index=True, max_length=100)),
                ('slug_group_responsible', models.SlugField(max_length=200, null=True, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Responsible',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_responsible', models.CharField(max_length=200, unique=True)),
                ('slug_responsible', models.SlugField(max_length=200, unique=True)),
                ('image1', models.ImageField(blank=True, null=True, upload_to='image_responsible/')),
                ('bio_responsible', models.TextField()),
                ('status_responsible', models.IntegerField(choices=[(0, 'غیر غعال'), (1, 'فعال')], default=0)),
                ('group_responsible', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='group_responsible', to='account.Group_Responsible')),
            ],
        ),
    ]
