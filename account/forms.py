from django import forms
from .models import CustomUser as User

JENS = [
    (0, "آقا"),
    (1, "خانوم")
]
ROLE = [
    (0, "مدد کار"),
    (1, "مدد جو")
]


class ImageForm(forms.Form):
    img_user = forms.ImageField(required=False, widget=forms.ClearableFileInput(
        attrs={'type': 'file', 'id': "imageUpload", 'accept': ".png, .jpg, .jpeg"}))

    username = forms.CharField(max_length=100, required=False, widget=forms.TextInput(
        attrs={'class': "single-input", "autocomplete": "off", "placeholder": "نام کاربری"}))

    first_name = forms.CharField(max_length=100, required=False, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "نام", "autocomplete": "off"}))

    last_name = forms.CharField(max_length=100, required=False, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "نام خانوادگی", "autocomplete": "off"}))

    phone_user = forms.CharField(max_length=100, required=False, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "شماره همراه", "autocomplete": "off"}))

    email = forms.CharField(max_length=100, required=False, widget=forms.TextInput(
        attrs={'type': "email", 'class': "single-input", "placeholder": "ایمیل ", "autocomplete": "off"}))

    comment_user = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': "single-textarea", "placeholder": "در صورت نیاز ، شرایط خود را ذکر کنید"}))

    last_name = forms.CharField(max_length=100, required=False, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "نام خانوادگی"}))

    password = forms.CharField(max_length=100, required=False, widget=forms.TextInput(
        attrs={'type': "password", 'class': "single-input", "placeholder": "رمز", "autocomplete": "off"}))

    confirm = forms.CharField(max_length=100, required=False,  widget=forms.TextInput(
        attrs={'type': "password", 'class': "single-input", "placeholder": "تکرار رمز", "autocomplete": "off"}))

    address_user = forms.CharField(max_length=100,  required=False, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "آدرس"}))

    role_user = forms.ChoiceField(

        choices=ROLE,
    )

    jens_user = forms.ChoiceField(

        choices=JENS,
    )
