from collections import OrderedDict
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import Group_Responsible, Responsible
from .models import CustomUser, Feedback


class CustomUser_Admin(admin.ModelAdmin):
    list_display = ['username', 'is_active',
                    'role_user', 'phone_user', 'confirm_role']
    list_editable = ['is_active', 'confirm_role']
    list_filter = ["confirm_role", 'is_active']
    search_fields = ['username']


admin.site.register(CustomUser, CustomUser_Admin)


class Responsible_Admin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ['name', 'status', 'group_responsible']
    list_filter = ("group_responsible", 'status')
    search_fields = ['name']


class Group_Admin(admin.ModelAdmin):
    prepopulated_fields = {
        'slug_group_responsible': ('name_group_responsible',)}


admin.site.register(Group_Responsible, Group_Admin)


admin.site.register(Responsible, Responsible_Admin)


class Feedback_Admin(admin.ModelAdmin):
    list_display = ['user', 'email', 'comment']
    search_fields = ['user', 'email', 'comment']


admin.site.register(Feedback, Feedback_Admin)
