from gol_boote.settings import AUTH_USER_MODEL as UserSend
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    CONFIRM_ROLE = (
        (0, "عادی"),
        (1, "مدد جو"),
        (2, "مدد کار"),
        (3, "مسئول"),
    )
    confirm_role = models.IntegerField(choices=CONFIRM_ROLE, default=0)

    img_user = models.ImageField(upload_to='profiles/', null=True, blank=True)

    JENS = (
        (0, "آقا"),
        (1, "خانوم")
    )

    ROLE = (
        (0, "مدد کار"),
        (1, "مدد جو")
    )

    role_user = models.IntegerField(choices=ROLE, default=0)

    jens_user = models.IntegerField(choices=JENS, default=0)
    comment_user = models.CharField(max_length=400, null=True, blank=True)
    address_user = models.CharField(max_length=400, null=True, blank=True)
    phone_user = models.CharField(max_length=14, null=True, blank=True)


class Group_Responsible(models.Model):
    name_group_responsible = models.CharField(max_length=100, db_index=True)
    slug_group_responsible = models.SlugField(
        max_length=200, unique=True, null=True)

    def __str__(self):
        return self.name_group_responsible


class Responsible(models.Model):
    STATUS = (
        (0, "غیر غعال"),
        (1, "فعال")
    )
    group_responsible = models.ForeignKey(
        Group_Responsible, on_delete=models.CASCADE, related_name='group')
    name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, unique=True)
    image = models.ImageField(
        upload_to='image_responsible/', blank=True, null=True)
    bio = models.TextField()
    status = models.IntegerField(choices=STATUS, default=0)
    telegram = models.CharField(max_length=100, null=True, blank=True)
    instagram = models.CharField(max_length=100, null=True, blank=True)
    email = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name


class Feedback (models.Model):
    email = models.EmailField(null=True, blank=True)
    user = models.ForeignKey(
        UserSend, on_delete=models.CASCADE, related_name='user_send', null=True, blank=True)
    comment = models.CharField(max_length=300)
