from django import forms
from .models import CustomUser as User

JENS = [
    (0, "آقا"),
    (1, "خانوم")
]
ROLE = [
    (0, "مدد کار"),
    (1, "مدد جو")
]


class ImageForm(forms.Form):
    img_user = forms.ImageField(required=False, widget=forms.ClearableFileInput(
        attrs={'type': 'file'}))

    username = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "نام کاربری"}))

    first_name = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "نام"}))

    last_name = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "نام خانوادگی"}))

    phone_user = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "شماره همراه"}))

    email = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'type': "email", 'class': "single-input", "placeholder": "ایمیل "}))

    comment_user = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "در صورت نیاز ، شرایط خود را ذکر کنید"}))

    last_name = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "نام خانوادگی"}))

    password = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'type': "password", 'class': "single-input", "placeholder": "رمز"}))

    confirm = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'type': "password", 'class': "single-input", "placeholder": "تکرار رمز"}))

    address_user = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'class': "single-input", "placeholder": "آدرس"}))

    role_user = forms.ChoiceField(

        choices=ROLE,
    )

    jens_user = forms.ChoiceField(

        choices=JENS,
    )
